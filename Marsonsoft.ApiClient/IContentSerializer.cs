﻿namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Represents an object capable of serializing string content.
    /// </summary>
    public interface IContentSerializer
    {
        /// <summary>
        /// Gets the Content-Type header value that this deserializer can serialize to, example: application/json
        /// </summary>
        string ContentType { get; }

        /// <summary>
        /// Serializes the object to string.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <returns>A string.</returns>
        string Serialize(object obj);
    }
}
