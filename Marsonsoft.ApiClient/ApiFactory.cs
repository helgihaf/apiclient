﻿using System;
using System.Net.Http;

namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Responsible for creating instances implementing <see cref="IApi"/>.
    /// </summary>
    public class ApiFactory : IApiFactory
    {
        private static readonly Lazy<ApiFactory> lazy = new Lazy<ApiFactory>(() => new ApiFactory());

        /// <summary>
        /// Gets the singleton instance of this class.
        /// </summary>
        public static ApiFactory Instance => lazy.Value;

        private ApiFactory()
        {
        }

        /// <summary>
        /// Creates an instance implementing <see cref="IApi"/> using the specified URI as a base URI.
        /// </summary>
        /// <param name="uri">The base URI to use.</param>
        /// <returns>An instance implementing <see cref="IApi"/>.</returns>
        public IApi Create(Uri uri)
        {
            return new Api(uri);
        }

        /// <summary>
        /// Creates an instance implementing <see cref="IApi"/> using the specified <see cref="HttpClient"/>.
        /// </summary>
        /// <param name="httpClient">The <see cref="HttpClient"/> to use.</param>
        /// <returns>An instance implementing <see cref="IApi"/>.</returns>
        public IApi Create(HttpClient httpClient)
        {
            return new Api(httpClient);
        }
    }

}
