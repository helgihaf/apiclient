﻿using System.Net.Http;

namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Represents an object that can intercept an API response.
    /// </summary>
    public interface IResponseInterceptor
    {
        /// <summary>
        /// Intercepts the specified response message.
        /// </summary>
        /// <param name="httpResponseMessage">The response message to intercept.</param>
        void Intercept(HttpResponseMessage httpResponseMessage);
    }
}
