﻿namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Content adaptar for JSON serialization.
    /// </summary>
    public class JsonContentAdapter : IContentSerializer, IContentDeserializer
    {
        private const string applicationJson = "application/json";
        
        /// <inheritdoc/>
        public string Accept => applicationJson;

        /// <inheritdoc/>
        public string ContentType => applicationJson;

        /// <inheritdoc/>
        public T Deserialize<T>(string content)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content);
        }

        /// <inheritdoc/>
        public string Serialize(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }
    }
}
