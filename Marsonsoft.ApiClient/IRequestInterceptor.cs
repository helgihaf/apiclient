﻿namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Represents an object that can intercept an <see cref="IApiRequest"/> for reading or modifying.
    /// </summary>
    public interface IRequestInterceptor
    {
        /// <summary>
        /// Intercepts the specified API request.
        /// </summary>
        /// <param name="apiRequest">The request to intercept.</param>
        void Intercept(IApiRequest apiRequest);
    }
}
