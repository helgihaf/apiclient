﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Marsonsoft.ApiClient
{
    internal class ApiRequest : IApiRequest
    {
        protected readonly HttpRequestMessage httpRequestMessage;
        protected bool ensureSuccessStatusCode;

        public ApiRequest(Api api, string path, HttpMethod httpMethod)
        {
            Api = api;
            Path = path;
            this.httpRequestMessage = new HttpRequestMessage
            {
                Method = httpMethod
            };
        }

        public Api Api { get; }
        public string Path { get; }

        public IApiRequest WithHeader(string name, string value)
        {
            httpRequestMessage.Headers.Add(name, value);
            return this;
        }

        public IApiRequest WithContent<T>(T content)
        {
            var serializer = Api.ContentSerializer;
            httpRequestMessage.Content = new StringContent(serializer.Serialize(content), Encoding.UTF8, serializer.ContentType);

            return this;
        }

        public IApiRequest WithHttpContent(HttpContent content)
        {
            httpRequestMessage.Content = content;

            return this;
        }

        public IApiRequest EnsureSuccessStatusCode()
        {
            ensureSuccessStatusCode = true;
            return this;
        }

        public async Task<HttpResponseMessage> AsRawAsync(CancellationToken cancellationToken)
        {
            if (Api.ContentDeserializers.Count > 0)
            {
                var acceptHeaderValues = httpRequestMessage.Headers.Accept;
                if (acceptHeaderValues == null || !acceptHeaderValues.Any())
                {
                    httpRequestMessage.Headers.Add("Accept", Api.ContentDeserializers.Keys);
                }
            }

            if (Api.RequestInterceptor != null)
            {
                Api.RequestInterceptor.Intercept(this);
            }

            httpRequestMessage.RequestUri = GetRequestUri();
            var httpResponseMessage = await Api.HttpClient.SendAsync(httpRequestMessage, cancellationToken);
            Api.LastResponse = new ApiResponse(Api, httpResponseMessage);

            if (Api.ResponseInterceptor != null)
            {
                Api.ResponseInterceptor.Intercept(httpResponseMessage);
            }

            if (ensureSuccessStatusCode)
            {
                httpResponseMessage.EnsureSuccessStatusCode();
            }

            return httpResponseMessage;
        }

        private Uri GetRequestUri()
        {
            var uri1 = Api.HttpClient.BaseAddress.ToString().TrimEnd('/');
            var uri2 = Path.TrimStart('/');
            return new Uri($"{uri1}/{uri2}");
        }

        public async Task<T> AsAsync<T>(CancellationToken cancellationToken)
        {
            var response = await AsRawAsync(cancellationToken);
            return await Api.ReadResponseAs<T>(response);
        }


        public async Task<string> AsStringAsync(CancellationToken cancellationToken)
        {
            var response = await AsRawAsync(cancellationToken);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
