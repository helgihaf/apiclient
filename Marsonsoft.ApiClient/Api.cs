﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Marsonsoft.ApiClient
{
    internal class Api : IApi
    {
        private static readonly HttpMethod httpMethodPatch = new HttpMethod("PATCH");
        private static readonly HttpMethod httpMethodDelete = new HttpMethod("DELETE");
        
        public Api(HttpClient httpClient)
        {
            HttpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            var jsonContentAdapter = new JsonContentAdapter();
            ContentDeserializers[jsonContentAdapter.Accept] = jsonContentAdapter;
            ContentSerializer = jsonContentAdapter;
        }

        public Api(Uri baseAddress) : this(new HttpClient())
        {
            HttpClient.BaseAddress = baseAddress;
        }

        internal HttpClient HttpClient { get; }

        internal IDictionary<string, IContentDeserializer> ContentDeserializers { get; }
            = new Dictionary<string, IContentDeserializer>(StringComparer.OrdinalIgnoreCase);

        internal IContentSerializer ContentSerializer { get; private set; }

        internal IRequestInterceptor RequestInterceptor { get; private set; }

        internal IResponseInterceptor ResponseInterceptor { get; private set; }

        public HttpRequestHeaders DefaultRequestHeaders => HttpClient.DefaultRequestHeaders;

        public IApiResponse LastResponse { get; internal set; }

        public void Use(IContentDeserializer contentDeserializer)
        {
            ContentDeserializers[contentDeserializer.Accept] = contentDeserializer;
        }

        public void Use(IContentSerializer contentSerializer) => ContentSerializer = contentSerializer;

        public void Use(IRequestInterceptor requestInterceptor) => RequestInterceptor = requestInterceptor;

        public void Use(IResponseInterceptor responseInterceptor) => ResponseInterceptor = responseInterceptor;

        public IApiRequest Get(string path)
        {
            return new ApiRequest(this, path, HttpMethod.Get);
        }

        public IApiRequest Post(string path)
        {
            return new ApiRequest(this, path, HttpMethod.Post);
        }

        public IApiRequest Put(string path)
        {
            return new ApiRequest(this, path, HttpMethod.Put);
        }

        public IApiRequest Patch(string path)
        {
            return new ApiRequest(this, path, httpMethodPatch);
        }

        public IApiRequest Delete(string path)
        {
            return new ApiRequest(this, path, httpMethodDelete);
        }

        internal async Task<T> ReadResponseAs<T>(HttpResponseMessage response)
        {
            string mediaType = response.Content?.Headers?.ContentType?.MediaType ?? "application/json";
            if (!ContentDeserializers.TryGetValue(mediaType, out IContentDeserializer contentDeserializer))
            {
                throw new InvalidOperationException($"Content deserializer not found for media type {mediaType}.");
            }
            
            string s = await response.Content.ReadAsStringAsync();
            return contentDeserializer.Deserialize<T>(s);
        }
    }
}
