﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Marsonsoft.ApiClient
{
    internal class ApiResponse : IApiResponse
    {
        private readonly Api api;

        public ApiResponse(Api api, HttpResponseMessage responseMessage)
        {
            this.api = api ?? throw new ArgumentNullException(nameof(api));
            ResponseMessage = responseMessage ?? throw new ArgumentNullException(nameof(responseMessage));
        }

        public HttpResponseMessage ResponseMessage { get; private set; }

        public Task<T> AsAsync<T>()
        {
            return api.ReadResponseAs<T>(ResponseMessage);
        }

        public Task<string> AsStringAsync()
        {
            return ResponseMessage.Content.ReadAsStringAsync();
        }
    }
}
