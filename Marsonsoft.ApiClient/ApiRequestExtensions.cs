﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Extension class for the <see cref="IApiRequest"/> interface.
    /// </summary>
    public static class ApiRequestExtensions
    {
        /// <summary>
        /// Executes the request asynchronously and returns a raw response message.
        /// </summary>
        /// <returns>A task of <see cref="HttpResponseMessage"/>.</returns>
        public static Task<HttpResponseMessage> AsRawAsync(this IApiRequest apiRequest)
        {
            return apiRequest.AsRawAsync(CancellationToken.None);
        }

        /// <summary>
        /// Executes the request asynchronously and returns the response as a string.
        /// </summary>
        /// <returns>A task of string.</returns>
        public static Task<string> AsStringAsync(this IApiRequest apiRequest)
        {
            return apiRequest.AsStringAsync(CancellationToken.None);
        }

        /// <summary>
        /// Executes the request asynchronously and returns the response as an object of type T.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>A task of T.</returns>
        public static Task<T> AsAsync<T>(this IApiRequest apiRequest)
        {
            return apiRequest.AsAsync<T>(CancellationToken.None);
        }


    }
}
