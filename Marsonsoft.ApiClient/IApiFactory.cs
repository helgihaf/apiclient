﻿using System;
using System.Net.Http;

namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// An interface for objects responsible for creating instances implementing <see cref="IApi"/>.
    /// </summary>
    public interface IApiFactory
    {
        /// <summary>
        /// Creates an instance implementing <see cref="IApi"/> using the specified URI as a base URI.
        /// </summary>
        /// <param name="uri">The base URI to use.</param>
        /// <returns>An instance implementing <see cref="IApi"/>.</returns>
        IApi Create(Uri uri);

        /// <summary>
        /// Creates an instance implementing <see cref="IApi"/> using the specified <see cref="HttpClient"/>.
        /// </summary>
        /// <param name="httpClient">The <see cref="HttpClient"/> to use.</param>
        /// <returns>An instance implementing <see cref="IApi"/>.</returns>
        IApi Create(HttpClient httpClient);
    }
}
