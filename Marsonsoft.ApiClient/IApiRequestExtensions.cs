﻿namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Extension methods for <see cref="IApiRequest"/>.
    /// </summary>
    public static class IApiRequestExtensions
    {
        /// <summary>
        /// Instructs the request to use the specified bearer token.
        /// </summary>
        /// <param name="apiRequest">The request.</param>
        /// <param name="token">The token to use.</param>
        /// <returns>An <see cref="IApiRequest"/>.</returns>
        /// <remarks>
        /// This is a convenience method that adds the "Authorization" HTTP header with the value of "Bearer " + token.
        /// </remarks>
        public static IApiRequest WithBearerToken(this IApiRequest apiRequest, string token)
        {
            return apiRequest.WithHeader("Authorization", "Bearer " + token);
        }
    }
}
