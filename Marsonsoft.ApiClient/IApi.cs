﻿using System.Net.Http.Headers;

namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Represents an external HTTP API.
    /// </summary>
    public interface IApi
    {
        /// <summary>
        /// Gets the headers which should be sent with each request.
        /// </summary>
        HttpRequestHeaders DefaultRequestHeaders { get; }

        /// <summary>
        /// Gets the last response received.
        /// </summary>
        IApiResponse LastResponse { get; }

        /// <summary>
        /// Instructs the <see cref="IApi"/> to use the specified content deserializer.
        /// </summary>
        /// <param name="contentDeserializer">The content deserializer to use.</param>
        /// <remarks>
        /// The default content deserializer is <see cref="JsonContentAdapter" />.
        /// </remarks>
        void Use(IContentDeserializer contentDeserializer);

        /// <summary>
        /// Instructs the <see cref="IApi"/> to use the specified content serializer.
        /// </summary>
        /// <param name="contentSerializer">The content serializer to use.</param>
        /// <remarks>
        /// The default content serializer is <see cref="JsonContentAdapter" />.
        /// </remarks>
        void Use(IContentSerializer contentSerializer);

        /// <summary>
        /// Instructs the <see cref="IApi"/> to use the specified request interceptor.
        /// </summary>
        /// <param name="requestInterceptor">The request interceptor to use.</param>
        void Use(IRequestInterceptor requestInterceptor);

        /// <summary>
        /// Instructs the <see cref="IApi"/> to use the specified response interceptor.
        /// </summary>
        /// <param name="responseInterceptor">The response interceptor to use.</param>
        void Use(IResponseInterceptor responseInterceptor);

        /// <summary>
        /// Creates a GET request for the specified path.
        /// </summary>
        /// <param name="path">The path to use in the GET request.</param>
        /// <returns>An instance implementing <see cref="IApiRequest"/>.</returns>
        IApiRequest Get(string path);


        /// <summary>
        /// Creates a POST request for the specified path.
        /// </summary>
        /// <param name="path">The path to use in the POST request.</param>
        /// <returns>An instance implementing <see cref="IApiRequest"/>.</returns>
        IApiRequest Post(string path);

        /// <summary>
        /// Creates a PUT request for the specified path.
        /// </summary>
        /// <param name="path">The path to use in the PUT request.</param>
        /// <returns>An instance implementing <see cref="IApiRequest"/>.</returns>
        IApiRequest Put(string path);

        /// <summary>
        /// Creates a PATCH request for the specified path.
        /// </summary>
        /// <param name="path">The path to use in the PATCH request.</param>
        /// <returns>An instance implementing <see cref="IApiRequest"/>.</returns>
        IApiRequest Patch(string path);

        /// <summary>
        /// Creates a DELETE request for the specified path.
        /// </summary>
        /// <param name="path">The path to use in the DELETE request.</param>
        /// <returns>An instance implementing <see cref="IApiRequest"/>.</returns>
        IApiRequest Delete(string path);
    }
}
