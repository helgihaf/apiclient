﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Represents the response of an API request.
    /// </summary>
    public interface IApiResponse
    {
        /// <summary>
        /// Gets the raw response message of the executed request.
        /// </summary>
        HttpResponseMessage ResponseMessage { get; }

        /// <summary>
        /// Fetches the response and returns it as a string.
        /// </summary>
        /// <returns>A task of string.</returns>
        Task<string> AsStringAsync();

        /// <summary>
        /// Fetches the response and returns it as an object of type T.
        /// </summary>
        /// <returns>A task of T.</returns>
        Task<T> AsAsync<T>();
    }
}
