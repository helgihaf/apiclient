﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Represents an API request.
    /// </summary>
    public interface IApiRequest
    {
        /// <summary>
        /// Instructs the request to use the specified HTTP header and value.
        /// </summary>
        /// <param name="name">The HTTP header name.</param>
        /// <param name="value">The header value.</param>
        /// <returns>An <see cref="IApiRequest"/>.</returns>
        IApiRequest WithHeader(string name, string value);

        /// <summary>
        /// Instructs the request to send the specified content with the request.
        /// </summary>
        /// <typeparam name="T">The type of the content.</typeparam>
        /// <param name="content">The content to send.</param>
        /// <returns>An <see cref="IApiRequest"/>.</returns>
        IApiRequest WithContent<T>(T content);

        /// <summary>
        /// Instructs the request to send the specified HTTP content with the request.
        /// </summary>
        /// <param name="content">The content to send.</param>
        /// <returns>An <see cref="IApiRequest"/>.</returns>
        IApiRequest WithHttpContent(HttpContent content);

        /// <summary>
        /// Throws an exception if the IsSuccessStatusCode property for the underlying HTTP response is false.
        /// </summary>
        /// <returns>An <see cref="IApiRequest"/>.</returns>
        /// <exception cref="HttpRequestException">The HTTP response is unsuccessful.</exception>
        IApiRequest EnsureSuccessStatusCode();

        /// <summary>
        /// Executes the request asynchronously and returns a raw response message.
        /// </summary>
        /// <returns>A task of <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> AsRawAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Executes the request asynchronously and returns the response as a string.
        /// </summary>
        /// <returns>A task of string.</returns>
        Task<string> AsStringAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Executes the request asynchronously and returns the response as an object of type T.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>A task of T.</returns>
        Task<T> AsAsync<T>(CancellationToken cancellationToken);
    }
}
