﻿namespace Marsonsoft.ApiClient
{
    /// <summary>
    /// Represents an object capable of deserializing string content.
    /// </summary>
    public interface IContentDeserializer
    {
        /// <summary>
        /// Gets the Accept header value that this deserializer can deserialize from, example: application/json
        /// </summary>
        string Accept { get; }

        /// <summary>
        /// Deserializes the specified content to an object of type T.
        /// </summary>
        /// <typeparam name="T">The type to deserialize to.</typeparam>
        /// <param name="content">The string content to deserialize from.</param>
        /// <returns>An object of type T.</returns>
        T Deserialize<T>(string content);
    }
}
