# API Client

This package implements a simple way to consume APIs. It is a simple wrapper on top of HttpClient. Features:
* GET, POST, PUT, PATCH and DELETE.
* Serialization and deserialization of objects using generics and extensible content serializer/deserializers.
* Defaults to Newtonsoft.Json serializer/deserializer.
* Intercept requests and responses.
* .NET Standard 2.0
* Full access to the HttpResponseMessage after the completion of an HTTP request.

## Installing
```powershell
Install-Package Marsonsoft.ApiClient
```

## Getting Started

### A simple GET for a string
```csharp
IApi api = ApiFactory.Instance.Create(new Uri("http://dnd5eapi.co/api/"));
var spell = await api
    .Get("spells/fireball")
    .EnsureSuccessStatusCode()
    .AsStringAsync();
```

### GET an object using a bearer token
```csharp
string token = await GetTokenAsync();

IApi api = ApiFactory.Instance.Create(new Uri("http://acme.customers.net/"));
var customer = await api
    .Get("customers/123")
    .WithBearerToken(token)
    .EnsureSuccessStatusCode()
    .AsAsync<Customer>();
```

### POST an object
```csharp
var customer = CreateCustomer();

IApi api = ApiFactory.Instance.Create(new Uri("http://acme.customers.net/"));
var customer = await api
    .Post("customers")
    .WithContent(customer)
    .EnsureSuccessStatusCode()
    .AsAsync<Customer>();
```

### GET and handle an error into an error object
```csharp
string token = await GetTokenAsync();

IApi api = ApiFactory.Instance.Create(new Uri("http://acme.customers.net/"));
Customer customer;
try
{
    customer = await api
        .Get("customers/123")
        .WithBearerToken(token)
        .EnsureSuccessStatusCode()
        .AsAsync<Customer>();
}
catch (System.Net.Http.HttpRequestException)
{
    Console.WriteLine($"Woops, got status code {api.LastResponse.ResponseMessage.StatusCode}");
    var error = await api.LastResponse.AsAsync<ErrorMessage>();
    // ...
}
```

### .NET Core Integration Testing
```csharp
var server = new TestServer(new WebHostBuilder()
           .UseStartup<Startup>());
var httpClient = server.CreateClient();
IApi api = ApiFactory.Instance.Create(httpClient);
var spell = await api
    .Get("spells/fireball")
    .EnsureSuccessStatusCode()
    .AsStringAsync();
```
See [Testing ASP.NET Core services and web apps](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/multi-container-microservice-net-applications/test-aspnet-core-services-web-apps)