﻿using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests for the PATCH HTTP method.
    /// </summary>
    public class PatchTests
    {
        [Fact]
        public async Task ShouldPatch_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            var result = await api.Patch("")
                .AsRawAsync();

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal("PATCH", webServer.Request.HttpMethod);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldPatch_WithContent_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            await api.Patch("")
                .WithContent(book)
                .AsRawAsync();

            await serviceRequest;
            Assert.Equal(TestData.GetBookJson().Content, webServer.Request.Content);
        }

        [Fact]
        public async Task ShouldPatch_WithHeader_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            var result = await api.Patch("")
                .WithHeader("Token", "Bearer smearer")
                .WithContent(book)
                .AsRawAsync();

            await serviceRequest;
            Assert.Equal("Bearer smearer", webServer.Request.Headers["Token"]);
        }

    }
}
