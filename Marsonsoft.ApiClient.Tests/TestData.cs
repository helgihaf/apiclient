﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Test data to be used for serialization tests.
    /// </summary>
    internal static class TestData
    {
        private static Lazy<string> bookJsonLazy = new Lazy<string>(() => Newtonsoft.Json.JsonConvert.SerializeObject(Book));

        public static readonly Book Book = new Book
        {
            Isbn = "978-1501161827",
            Title = "The Dark Tower III: The Waste Lands",
            Authors = new[] { "Stephen King" }
        };

        public static WebServerContent GetBookJson()
        {
            return new WebServerContent
            {
                MediaType = "application/json",
                Content = bookJsonLazy.Value
            };
        }

        public static WebServerContent GetBookXml()
        {
            var content = new WebServerContent
            {
                MediaType = "application/xml"
            };

            var xmlSerializer = new XmlSerializer(typeof(Book));
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, Book);
                content.Content = textWriter.ToString();
            }

            return content;
        }

        public const string RawXml = "<name>Helgi Hafþórsson</name>";
    }
}
