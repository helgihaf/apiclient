﻿using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// A micro web server used for unit testing.
    /// </summary>
    internal class WebServer
    {
        public string Url { get; }
        public WebServerContent Output { get; }
        public HttpStatusCode HttpStatusCode { get; }
        public WebServerRequest Request { get; private set; }

        public static string GetLocalhostAddress()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            int port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();

            return $"http://localhost:{port}/";
        }
        
        public WebServer(string url, WebServerContent output, HttpStatusCode httpStatusCode = HttpStatusCode.OK)
        {
            Url = url;
            Output = output;
            HttpStatusCode = httpStatusCode;
        }

        public Task StartServiceRequest()
        {
            return Task.Run(() =>
            {
                var listener = new HttpListener();
                listener.Prefixes.Add(Url);
                listener.Start();

                // GetContext method blocks while waiting for a request. 
                HttpListenerContext context = listener.GetContext();

                Request = new WebServerRequest
                {
                    HttpMethod = context.Request.HttpMethod,
                    Headers = context.Request.Headers,
                    Url = context.Request.Url
                };

                if (context.Request.InputStream != null)
                {
                    using (var reader = new StreamReader(context.Request.InputStream))
                    {
                        Request.Content = reader.ReadToEnd();
                    }
                }

                HttpListenerResponse response = context.Response;

                if (HttpStatusCode != HttpStatusCode.OK)
                {
                    response.StatusCode = (int)HttpStatusCode;
                }

                if (Output?.MediaType != null)
                {
                    response.Headers.Add(HttpResponseHeader.ContentType, Output.MediaType);
                }

                Stream stream = response.OutputStream;
                var writer = new StreamWriter(stream);
                if (Output != null)
                {
                    writer.Write(Output.Content);
                }
                writer.Close();
            });
        }
    }
}
