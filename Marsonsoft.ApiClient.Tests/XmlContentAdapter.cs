﻿using System.IO;
using System.Xml.Serialization;

namespace Marsonsoft.ApiClient.Tests
{
    public class XmlContentAdapter : IContentSerializer, IContentDeserializer
    {
        private const string applicationXml = "application/xml";

        public string Accept => applicationXml;

        public string ContentType => applicationXml;

        public T Deserialize<T>(string content)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var textReader = new StringReader(content))
            {
                return (T)xmlSerializer.Deserialize(textReader);
            }
        }

        public string Serialize(object obj)
        {
            var xmlSerializer = new XmlSerializer(obj.GetType());
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, obj);
                return textWriter.ToString();
            }
        }
    }
}
