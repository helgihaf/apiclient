﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests for the POST HTTP method.
    /// </summary>
    public class PostTests
    {
        [Fact]
        public async Task ShouldPost_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookJson(), HttpStatusCode.Created);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            var result = await api.Post("")
                .AsRawAsync();

            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Equal("POST", webServer.Request.HttpMethod);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldPost_WithContent_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookJson(), HttpStatusCode.Created);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            var result = await api.Post("")
                .WithContent(book)
                .AsRawAsync();

            await serviceRequest;
            Assert.Equal(TestData.GetBookJson().Content, webServer.Request.Content);
        }

        [Fact]
        public async Task ShouldPostAndGetNewObject_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookJson(), HttpStatusCode.Created);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            var newBook = await api.Post("")
                .WithContent(book)
                .EnsureSuccessStatusCode()
                .AsAsync<Book>();

            Assert.Equal(book.Isbn, newBook.Isbn);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldPost_WithHeader_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookJson(), HttpStatusCode.Created);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            await api.Post("")
                .WithHeader("Token", "Bearer smearer")
                .AsRawAsync();

            await serviceRequest;
            Assert.Equal("Bearer smearer", webServer.Request.Headers["Token"]);
        }

        [Fact]
        public async Task ShouldPost_WithHttpContent_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookJson(), HttpStatusCode.Created);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var xml = TestData.RawXml;

            await api.Post("")
                .WithHttpContent(new StringContent(xml, Encoding.UTF8, "application/xml"))
                .AsRawAsync();

            await serviceRequest;
            Assert.Equal(xml, webServer.Request.Content);
            Assert.Equal("application/xml; charset=utf-8", webServer.Request.Headers["Content-Type"]);
        }

    }
}
