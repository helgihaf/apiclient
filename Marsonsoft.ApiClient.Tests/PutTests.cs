﻿using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests for the PUT HTTP method.
    /// </summary>
    public class PutTests
    {
        [Fact]
        public async Task ShouldPut_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var result = await api.Put("")
                .AsRawAsync();

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal("PUT", webServer.Request.HttpMethod);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldPut_WithContent_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            await api.Put("")
                .WithContent(book)
                .AsRawAsync();

            Assert.Equal(TestData.GetBookJson().Content, webServer.Request.Content);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldPut_WithHeader_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            await api.Put("")
                .WithHeader("Token", "Bearer smearer")
                .WithContent(book)
                .AsRawAsync();

            await serviceRequest;
            Assert.Equal("Bearer smearer", webServer.Request.Headers["Token"]);
        }

    }
}
