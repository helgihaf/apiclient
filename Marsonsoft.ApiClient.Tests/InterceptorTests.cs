﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests for the <see cref="IRequestInterceptor"/> and <see cref="IResponseInterceptor"/> mechanics.
    /// </summary>
    public class InterceptorTests
    {
        [Fact]
        public async Task ShouldUseRequestInterceptor_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));

            var requestInterceptor = new RequestInterceptor();
            api.Use(requestInterceptor);

            var result = await api.Put("")
                .AsRawAsync();

            Assert.NotNull(webServer.Request.Headers.AllKeys.FirstOrDefault(k => k == "X-Logger-Activity-Chain-Id"));
            Assert.NotNull(webServer.Request.Headers.AllKeys.FirstOrDefault(k => k == "X-Logger-Activity-Id"));

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldUseResponseInterceptor_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookJson());

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));

            var responseInterceptor = new ResponseInterceptor();
            api.Use(responseInterceptor);

            var result = await api.Get("")
                .AsRawAsync();

            await serviceRequest;

            Assert.True(responseInterceptor.Invoked);
        }
    }

    internal class RequestInterceptor : IRequestInterceptor
    {
        public void Intercept(IApiRequest apiRequest)
        {
            apiRequest.WithHeader("X-Logger-Activity-Chain-Id", Guid.NewGuid().ToString());
            apiRequest.WithHeader("X-Logger-Activity-Id", Guid.NewGuid().ToString());
        }
    }

    internal class ResponseInterceptor : IResponseInterceptor
    {
        public bool Invoked { get; private set; }

        public void Intercept(HttpResponseMessage httpResponseMessage)
        {
            Invoked = true;
        }
    }
}
