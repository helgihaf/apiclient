﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// This file is here to provide a dummy implementation of the public interfaces. If we change the interfaces, these
// implementations will break, signalling a major version upgrade.
namespace Marsonsoft.ApiClient.Tests
{
    class ApiCheck : IApi
    {
        public HttpRequestHeaders DefaultRequestHeaders => throw new NotImplementedException();

        public IApiResponse LastResponse => throw new NotImplementedException();

        public IApiRequest Delete(string path)
        {
            throw new NotImplementedException();
        }

        public IApiRequest Get(string path)
        {
            throw new NotImplementedException();
        }

        public IApiRequest Patch(string path)
        {
            throw new NotImplementedException();
        }

        public IApiRequest Post(string path)
        {
            throw new NotImplementedException();
        }

        public IApiRequest Put(string path)
        {
            throw new NotImplementedException();
        }

        public void Use(IContentDeserializer contentDeserializer)
        {
            throw new NotImplementedException();
        }

        public void Use(IContentSerializer contentSerializer)
        {
            throw new NotImplementedException();
        }

        public void Use(IRequestInterceptor requestInterceptor)
        {
            throw new NotImplementedException();
        }

        public void Use(IResponseInterceptor responseInterceptor)
        {
            throw new NotImplementedException();
        }
    }

    class ApiFactoryCheck : IApiFactory
    {
        public IApi Create(Uri uri)
        {
            throw new NotImplementedException();
        }

        public IApi Create(HttpClient httpClient)
        {
            throw new NotImplementedException();
        }
    }

    class ApiRequestsCheck : IApiRequest
    {
        public Task<T> AsAsync<T>(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<HttpResponseMessage> AsRawAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> AsStringAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public IApiRequest EnsureSuccessStatusCode()
        {
            throw new NotImplementedException();
        }

        public IApiRequest WithContent<T>(T content)
        {
            throw new NotImplementedException();
        }

        public IApiRequest WithHeader(string name, string value)
        {
            throw new NotImplementedException();
        }

        public IApiRequest WithHttpContent(HttpContent content)
        {
            throw new NotImplementedException();
        }
    }

    class ApiResponseCheck : IApiResponse
    {
        public HttpResponseMessage ResponseMessage => throw new NotImplementedException();

        public Task<T> AsAsync<T>()
        {
            throw new NotImplementedException();
        }

        public Task<string> AsStringAsync()
        {
            throw new NotImplementedException();
        }
    }

    class ContentDeserializerCheck : IContentDeserializer
    {
        public string Accept => throw new NotImplementedException();

        public T Deserialize<T>(string content)
        {
            throw new NotImplementedException();
        }
    }

    class ContentSerializerCheck : IContentSerializer
    {
        public string ContentType => throw new NotImplementedException();

        public string Serialize(object obj)
        {
            throw new NotImplementedException();
        }
    }

    class RequestInterceptorCheck : IRequestInterceptor
    {
        public void Intercept(IApiRequest apiRequest)
        {
            throw new NotImplementedException();
        }
    }

    class ResponseInterceptorCheck : IResponseInterceptor
    {
        public void Intercept(HttpResponseMessage httpResponseMessage)
        {
            throw new NotImplementedException();
        }
    }
}
