﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests that demonstrate an alternitive serialization method than the default JSON serialization.
    /// </summary>
    public class SerializationTests
    {
        [Fact]
        public async Task ShouldUseXmlDeserialization_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookXml());

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var contentAdapter = new XmlContentAdapter();
            api.Use((IContentDeserializer)contentAdapter);
            var book = await api.Get("").AsAsync<Book>();
            Assert.NotNull(book);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldUseXmlSerialization_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookXml(), HttpStatusCode.Created);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));

            var contentAdapter = new XmlContentAdapter();
            api.Use((IContentDeserializer)contentAdapter);
            api.Use((IContentSerializer)contentAdapter);

            var book = TestData.Book;
            var newBook = await api.Post("")
                .WithContent(TestData.Book)
                .EnsureSuccessStatusCode()
                .AsAsync<Book>();

            Assert.Equal(TestData.GetBookXml().Content, webServer.Request.Content);
            Assert.Equal(book.Isbn, newBook.Isbn);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldUseXmlSerialization_OnException_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookXml(), HttpStatusCode.BadRequest);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));

            var contentAdapter = new XmlContentAdapter();
            api.Use((IContentDeserializer)contentAdapter);
            api.Use((IContentSerializer)contentAdapter);

            try
            {
                var book = await api.Get("")
                    .EnsureSuccessStatusCode()
                    .AsAsync<Book>();
                throw new Exception("This should not happen");
            }
            catch (HttpRequestException)
            {
                Assert.Equal(HttpStatusCode.BadRequest, api.LastResponse.ResponseMessage.StatusCode);
                var error = await api.LastResponse.AsAsync<Book>();
                Assert.NotNull(error);
            }

            await serviceRequest;
        }

    }
}
