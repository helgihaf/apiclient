using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests for the GET HTTP method.
    /// </summary>
    public class GetTests
    {
        private readonly ITestOutputHelper output;

        public GetTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public async Task ShouldGetRaw_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());
            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var request = await api.Get("").AsRawAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            
            await serviceRequest;
        }

        [Fact]
        public async Task ShouldGetRaw_WithHttpClient_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());
            var serviceRequest = webServer.StartServiceRequest();
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(webServer.Url);
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var request = await api.Get("").AsRawAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldGetString_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());
            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var response = await api.Get("").AsStringAsync();
            Assert.True(response.Length > 1);
            
            await serviceRequest;
        }

        [Fact]
        public async Task ShouldGetObject_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());
            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = await api.Get("").AsAsync<Book>();
            Assert.NotNull(book);
            
            await serviceRequest;
        }

        [Fact]
        public async Task ShouldGetWithHeader_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var response = await api.Get("")
                .WithHeader("Token", "Bearer smearer")
                .AsStringAsync();
            output.WriteLine(response);

            await serviceRequest;
            Assert.Equal("Bearer smearer", webServer.Request.Headers["Token"]);
        }

        [Fact]
        public async Task ShouldGetExpectSuccess_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var response = await api.Get("")
                .WithHeader("Token", "Bearer smearer")
                .EnsureSuccessStatusCode()
                .AsStringAsync();
            output.WriteLine(response);

            await serviceRequest;
            Assert.Equal("Bearer smearer", webServer.Request.Headers["Token"]);
        }

        [Fact]
        public async Task ShouldGet_BadRequest_EmptyResult()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null, HttpStatusCode.BadRequest);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var response = await api.Get("")
                .AsStringAsync();
            Assert.Empty(response);

            await serviceRequest;
        }


        [Fact]
        public async Task ShouldGetExcpectSuccess_BadRequest_Exception()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null, HttpStatusCode.BadRequest);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            await Assert.ThrowsAsync<HttpRequestException>(
                async () => await api.Get("")
                    .EnsureSuccessStatusCode()
                    .AsStringAsync());

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldGetExcpectSuccess_BadRequestBook_Exception()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, TestData.GetBookJson(), HttpStatusCode.BadRequest);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            try
            {
                var book = await api.Get("")
                    .EnsureSuccessStatusCode()
                    .AsAsync<Book>();
                throw new Exception("This should not happen");
            }
            catch (HttpRequestException)
            {
                Assert.Equal(HttpStatusCode.BadRequest, api.LastResponse.ResponseMessage.StatusCode);
                var error = await api.LastResponse.AsAsync<Book>();
                Assert.NotNull(error);
                Assert.Equal(TestData.Book.Isbn, error.Isbn);
            }

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldGetObject_SetDefaultAcceptContent_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());
            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            api.DefaultRequestHeaders.Add("Token", "Bearer smearer");
            var book = await api.Get("").AsAsync<Book>();
            Assert.NotNull(book);

            await serviceRequest;
            Assert.Equal("Bearer smearer", webServer.Request.Headers["Token"]);
        }

        [Fact]
        public async Task ShouldGetObject_UsesDefaultContentAdapter()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());
            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = await api.Get("").AsAsync<Book>();
            Assert.NotNull(book);

            await serviceRequest;
            Assert.Equal("application/json", webServer.Request.Headers["Accept"]);
        }

        [Fact]
        public async Task ShouldGetResource_Ok()
        {
            var webServer = CreateWebServer(TestData.GetBookJson());
            var serviceRequest = webServer.StartServiceRequest();
            var fullUrl = new Uri(webServer.Url + "Things/v2");
            IApi api = ApiFactory.Instance.Create(fullUrl);
            var request = await api.Get("environments").AsRawAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.Equal("/Things/v2/environments", webServer.Request.Url.LocalPath);

            await serviceRequest;
        }


        private WebServer CreateWebServer(WebServerContent output)
        {
            string url = WebServer.GetLocalhostAddress();
            return new WebServer(url, output);
        }
    }
}
