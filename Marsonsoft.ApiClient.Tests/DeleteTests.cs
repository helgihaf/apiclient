﻿using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests for the DELETE HTTP method.
    /// </summary>
    public class DeleteTests
    {
        [Fact]
        public async Task ShouldDelete_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            var result = await api.Delete("")
                .AsRawAsync();

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal("DELETE", webServer.Request.HttpMethod);

            await serviceRequest;
        }

        [Fact]
        public async Task ShouldDelete_WithHeader_Ok()
        {
            string url = WebServer.GetLocalhostAddress();
            var webServer = new WebServer(url, null);

            var serviceRequest = webServer.StartServiceRequest();
            IApi api = ApiFactory.Instance.Create(new Uri(webServer.Url));
            var book = TestData.Book;
            var result = await api.Delete("")
                .WithHeader("Token", "Bearer smearer")
                .AsRawAsync();

            await serviceRequest;
            Assert.Equal("Bearer smearer", webServer.Request.Headers["Token"]);
        }
    }
}
