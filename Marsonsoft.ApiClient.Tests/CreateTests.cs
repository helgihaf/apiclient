﻿using System;
using System.Net.Http;
using Xunit;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// Tests for creation of IApi
    /// </summary>
    public class CreateTests
    {
        [Fact]
        public void ShouldCreateApi_WithUrl_Ok()
        {
            IApi api = ApiFactory.Instance.Create(new Uri("http://www.api.com"));
        }

        [Fact]
        public void ShouldCreateApi_WithHttpClient_Ok()
        {
            var httpClient = new HttpClient();
            IApi api = ApiFactory.Instance.Create(httpClient);
        }
    }
}
