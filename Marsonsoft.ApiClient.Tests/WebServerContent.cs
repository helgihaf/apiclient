﻿namespace Marsonsoft.ApiClient.Tests
{
    public class WebServerContent
    {
        public string MediaType { get; set; }
        public string Content { get; set; }
    }
}
