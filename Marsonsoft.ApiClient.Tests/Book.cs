﻿namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// A book class that is used to test serializations.
    /// </summary>
    public class Book
    {
        public string Isbn { get; set; }
        public string Title { get; set; }
        public string[] Authors { get; set; }
    }
}
