﻿using System;
using System.Collections.Specialized;

namespace Marsonsoft.ApiClient.Tests
{
    /// <summary>
    /// A web server request used by <see cref="WebServer"/>.
    /// </summary>
    public class WebServerRequest
    {
        public string HttpMethod { get; set; }
        public NameValueCollection Headers { get; set; }
        public string Content { get; set; }
        public Uri Url { get; set; }
    }
}
